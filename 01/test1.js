/* Example 1 */
exports.name = function() {
    console.log('I am banana');
}
/* Example 2 */
exports.hi = function() {
    console.log('Hihi');
}
/* Example 3 */
module.exports = {
    type: 'string',
    text: 'hihi',
}
//注意:如果写出了module.exports 会忽略掉上面写的exports.hi 跟 exports.name